#!/usr/bin/env python
 
"""IMAP to piler importer. Heavily inspired in imapbackup """
__version__ = "0.0"
__author__ = "javivf@gmail.com"
__copyright__ = "GPL v3"
    
import getpass, os, gc, sys, time, platform, getopt
import mailbox, imaplib, socket
import re, hashlib, gzip, bz2
import email 
from time import mktime
from datetime import datetime, date, timedelta
from subprocess import call

# Regular expressions for parsing
MSGID_RE = re.compile("^Message\-Id\: (.+)", re.IGNORECASE + re.MULTILINE)
BLANKS_RE = re.compile(r'\s+', re.MULTILINE)
 
# Constants
UUID = '19AF1258-1AAF-44EF-9D9A-731079D6FAD7' # Used to generate Message-Ids

class SkipFolderException(Exception):
    """Indicates aborting processing of current folder, continue with next folder."""
    pass

def download_messages(server, filename, messages, config):
    """Download messages from folder and append to mailbox"""
    
    # Open disk file
    mbox = file(filename, 'ab')
    
    # the folder has already been selected by scanFolder()
    # nothing to do
    if not len(messages):
        #print "New messages: 0"
        mbox.close()
        return
    
    total = biggest = 0
    count = 1
    to_delete = [] 
    
    # each new message
    for msg_id in messages.keys():
        if count%250==0:
            print "%s messages downloaded..." % (count)

        # This "From" and the terminating newline below delimit messages
        # in mbox files
        buf = "From nobody %s\n" % time.strftime('%a %m %d %H:%M:%S %Y')
        # If this is one of our synthesised Message-IDs, insert it before
        # the other headers
        if UUID in msg_id:
            buf = buf + "Message-Id: %s\n" % msg_id
        
        mbox.write(buf)
        
        # fetch message
        typ, data = server.fetch(messages[msg_id], "RFC822")
        assert('OK' == typ)

        if config['remove'] and typ=='OK':
	    to_delete.append(messages[msg_id])
        
        text = data[0][1].strip().replace('\r','')
        
        mbox.write(text)
        mbox.write('\n\n')
        
        size = len(text)
        biggest = max(size, biggest)
        total += size
        
        del data
        gc.collect()
	count += 1
    
    mbox.close()
    print "    %s total, %s for largest message" % (total,biggest)
    return to_delete

def delete_messages(foldername, to_delete):
    s = connect_and_login(config)
    s.select(foldername)
    for id in to_delete:
        s.store(id, '+FLAGS', '\\Deleted')
    s.expunge()
    s.close()
    s.logout()

def normalize_date(data):
    tmp = " ".join(data.split()).split()
    if  tmp.__len__()>=6:
	return datetime.fromtimestamp(mktime(time.strptime(tmp[1] + " " + tmp[2] + " " + tmp[3] + " " + tmp[4], "%d %b %Y %H:%M:%S")))
    if tmp.__len__()==5:
	#print data
	return datetime.fromtimestamp(mktime(time.strptime(tmp[0] + " " + tmp[1] + " " + tmp[2] + " " + tmp[3], "%d %b %Y %H:%M:%S")))


def scan_folder(server, foldername):
    """Gets IDs of messages in the specified folder, returns id:num dict"""
    messages = {}
    try:
        typ, data = server.select(foldername, readonly=True)
        if 'OK' != typ:
            raise SkipFolderException("SELECT failed: %s" % (data))
        
        num_msgs = int(data[0])

        parser= email.Parser.HeaderParser()
        
        # each message
        for num in range(1, num_msgs):
            if num%250==0:
	        print "%s messages found..." % (num)

            # Retrieve Message-Id, making sure we don't mark all messages as read
            typ, data = server.fetch(num, '(BODY.PEEK[HEADER.FIELDS (MESSAGE-ID DATE)])')
            if 'OK' != typ:
                raise SkipFolderException("FETCH %s failed: %s" % (num, data))
            ##header = data[0][1].strip()
            # remove newlines inside Message-Id (a dumb Exchange trait)
            ## header = BLANKS_RE.sub(' ', header)
            try:
                msg_id = parser.parsestr(data[0][1])['Message-id']
                dt = normalize_date(parser.parsestr(data[0][1])['Date'])
                if ((msg_id not in messages.keys()) and (dt.date() <= date.today()-timedelta(days=int(config['days'])))):
                    # avoid adding dupes
                    messages[msg_id] = num
                    
            except (IndexError, AttributeError):
                # Some messages may have no Message-Id, so we'll synthesise one
                # (this usually happens with Sent, Drafts and .Mac news)
                typ, data = server.fetch(num, '(BODY[HEADER.FIELDS (FROM TO CC DATE SUBJECT)])')
                if 'OK' != typ:
                    raise SkipFolderException("FETCH %s failed: %s" % (num, data))
                header = data[0][1].strip()
                header = header.replace('\r\n','\t')
                messages['<' + UUID + '.' + hashlib.sha1(header).hexdigest() + '>'] = num
    finally:
        print " ",
    
    # done
    print "%d messages in %s folder" % (len(messages.keys()), foldername)
    return messages

def parse_paren_list(row):
    """Parses the nested list of attributes at the start of a LIST response"""
    # eat starting paren
    assert(row[0] == '(')
    row = row[1:]
    
    result = []
    
    # NOTE: RFC3501 doesn't fully define the format of name attributes
    
    name_attrib_re = re.compile("^\s*(\\\\[a-zA-Z0-9_]+)\s*")
    
    # eat name attributes until ending paren
    while row[0] != ')':
        # recurse
        if row[0] == '(':
            paren_list, row = parse_paren_list(row)
            result.append(paren_list)
        # consume name attribute
        else:
            match = name_attrib_re.search(row)
            assert(match != None)
            name_attrib = row[match.start():match.end()]
            row = row[match.end():]
            #print "MATCHED '%s' '%s'" % (name_attrib, row)
            name_attrib = name_attrib.strip()
            result.append(name_attrib)
    # eat ending paren
    assert(')' == row[0])
    row = row[1:]
    
    # done!
    return result, row
  
def parse_string_list(row):
    """Parses the quoted and unquoted strings at the end of a LIST response"""
    slist = re.compile('\s*(?:"([^"]+)")\s*|\s*(\S+)\s*').split(row)
    return [s for s in slist if s]

def parse_list(row):
    """Prases response of LIST command into a list"""
    row = row.strip()
    paren_list, row = parse_paren_list(row)
    string_list = parse_string_list(row)
    assert(len(string_list) == 2)
    return [paren_list] + string_list

def get_hierarchy_delimiter(server):
    """Queries the imapd for the hierarchy delimiter, eg. '.' in INBOX.Sent"""
    # see RFC 3501 page 39 paragraph 4
    typ, data = server.list('', '')
    assert(typ == 'OK')
    assert(len(data) == 1)
    lst = parse_list(data[0]) # [attribs, hierarchy delimiter, root name]
    hierarchy_delim = lst[1]
    # NIL if there is no hierarchy
    if 'NIL' == hierarchy_delim:
        hierarchy_delim = '.'
    return hierarchy_delim

def get_names(server):
    """Get list of folders, returns [(FolderName,FileName)]"""
    # Get hierarchy delimiter
    delim = get_hierarchy_delimiter(server)
    
    # Get LIST of all folders
    typ, data = server.list()
    assert(typ == 'OK')
 
    names = []
    
    # parse each LIST, find folder name
    for row in data:
        lst = parse_list(row)
        foldername = lst[2]
        filename = '.'.join(foldername.split(delim)) + '.mbox'
        # print "\n*** Folder:", foldername # *DEBUG
        # print "***   File:", filename # *DEBUG
        names.append((foldername, filename))
    # done
    print "%s folders found" % (len(names))
    return names

def connect_and_login(config):
    """Connects to the server and logs in.  Returns IMAP4 object."""
 
    if config['usessl']:
        print "Connecting to '%s' TCP port %d, SSL" % (config['server'], config['port'])
        server = imaplib.IMAP4_SSL(config['server'], config['port'])
    else:
        print "Connecting to '%s' TCP port %d" % (config['server'], config['port'])
        server = imaplib.IMAP4(config['server'], config['port'])
 
    # speed up interactions on TCP connections using small packets
    server.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    print "Logging in as '%s'" % (config['user'])
    server.login(config['user'], config['pass'])
    
    return server

def print_error_exit():
    print("Usage: %s -s HOST -u USERNAME -p PASSWORD [-d DAYSOLD] [-e] [-r]" % sys.argv[0])
    print(" to use SSL connection add -e")
    print(" to remove messages on remote server add -r")
    sys.exit(2)

def get_config():
    try:
        myopts, args = getopt.getopt(sys.argv[1:],"s:u:p:d:er")
        
    except getopt.GetoptError as e:
        print (str(e))
        print_error_exit()
 
    for o, a in myopts:
        if o == '-s':
            config['server']=a
            if ':' in config['server']:
                config['server'] = a.split(':')[0]
                config['port'] = int(a.split(':')[1])
                
        elif o == '-u':
            config['user']=a
        elif o == '-p':
            config['pass']=a
        elif o == '-d':
            try:
                config['days']=int(a)
            except:
                config['days']=a
        elif o == '-e':
            config['usessl']=True
        elif o == '-r':
            config['remove']=True
    
    if (config['usessl'] and config['port'] == 143):
        config['port'] = 993

if __name__ == '__main__':
    if getpass.getuser()!='piler':
	print("You must run this script as 'piler'")
        sys.exit(2)

    config = {'server':'', 'port':143, 'user':'', 'pass':'', 'usessl':False, 'days': 0, 'remove':False} 
    get_config()
   
    if config['server'] == '' or config['user'] == '' or config['pass'] == '' or not(isinstance(config['days'], int)):
        print_error_exit()
 
    server = connect_and_login(config)
    names = get_names(server)
    
    for name_pair in names:
        try:
            foldername, filename = name_pair
	    print "Scanning %s folder" % (foldername)
            fol_messages = scan_folder(server, foldername)
            
            if len(fol_messages)>0:
	        print "Downloading %s folder" % (foldername)
                to_delete = download_messages(server, filename, fol_messages, config)
                delete_messages(foldername, to_delete)
	        print "Importing %s " % (filename)
                call(["pilerimport", "-m", filename]) 
            
        except SkipFolderException, e:
            print e

    server.close()
    server.logout()
